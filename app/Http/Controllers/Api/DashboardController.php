<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Traits\CommonTrait;


class DashboardController extends Controller
{
    public function __construct() {
        $this->User = new User();
        $this->ENCRPT_KEY = env('ENCRPT_KEY');
        $this->ENCRPT_IV = env('ENCRPT_IV');
    }

    use CommonTrait;
    public function UserList(Request $request){
        $users = $this->User->getUserList();
        $user_list = array();
        foreach($users as $key => $user){
            $user_list[] = array("user_name"=>$user->name,'email'=>$user->email) ;
        }

        if(empty($user_list)){
            $result = array('status'=>0,"message"=>'Users not found','response'=>$user_list);
        } else {
            $result = array('status'=>1,"message"=>'success','response'=>$user_list);
        }
        $encrypt_data = $this->EncryptData($result);
        return $encrypt_data;
    }
}
