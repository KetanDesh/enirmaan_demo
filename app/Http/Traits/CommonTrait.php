<?php
namespace App\Http\Traits;
use Illuminate\Http\Request;

trait CommonTrait{
        public function EncryptData($data){
                $data = json_encode($data);
                return openssl_encrypt($data,"AES-256-CBC",$this->ENCRPT_KEY ,NULL,$this->ENCRPT_IV); 
        }
}
