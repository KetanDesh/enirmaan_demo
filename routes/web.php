<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::controller(AuthController::class)->group(function() {
    Route::get('login','index')->name('login');
    Route::post('post-login', 'LoginCustomer')->name('login.post'); 
    Route::get('registration',  'registration')->name('register');
    Route::post('post-registration', 'postRegistration')->name('register.post'); 
    Route::get('dashboard', 'dashboard'); 
    Route::get('logout', 'logout')->name('logout');
});