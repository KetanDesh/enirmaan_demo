@extends('dashboard/layout')
  
@section('content')

<div class="container" style="padding-top: 20px;">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Register Users') }}</div>
  
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <!-- User register table -->
                    <div class="container"> 
                        <div id="error" style="display:none;">Records not Found</div>       
                          <table class="table table-bordered" id="CustomeTable">
                            <thead>
                              <tr>
                                <th>Customer Name</th>
                                <th>Email</th>
                              </tr>
                            </thead>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    
    <script>
    $(document).ready(function() {

    $.ajax({url: "/api/users_list", 
        success: function(result) {
            var decrypted = DecryptData(result);
            var js = JSON.parse(decrypted);
            var json = js.response;
            var status = js.status;

            if(status == 1){
               $.each(json, function(key, value){
                    $(" #CustomeTable").append("<tr><td>" +
                                         value.email +"</td><td>" +
                                         value.user_name + 
                                        "</td><tr>");
                });
            } else {
                $('#error').css("display", "block");
            }              
        }});
    });
    </script>
@endsection